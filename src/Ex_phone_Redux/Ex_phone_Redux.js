import React, { Component } from "react";
import Detail from "./Detail";
import ListRenderPhone from "./ListRenderPhone";

export default class Ex_phone_Redux extends Component {
  render() {
    console.log(this.props);
    return (
      <div className="container">
        <h2>Bài tập Phone Redux</h2>
        <ListRenderPhone />
        <Detail />
      </div>
    );
  }
}
