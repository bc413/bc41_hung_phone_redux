import React, { Component } from "react";
import { connect } from "react-redux";

class Detail extends Component {
  render() {
    let {
      maSP,
      hinhAnh,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
    } = this.props.phone;
    console.log(this.props.phone);
    return (
      <div className="row">
        <div className="col-5">
          <img style={{ width: "100%" }} src={hinhAnh} alt="" />
        </div>
        <div className="col-7 mt-5 text-left">
          <p>
            <strong>Mã sản phẩm:</strong> {maSP}
          </p>
          <p>
            <strong>Tên sản phẩm:</strong> {tenSP}
          </p>
          <p>
            <strong>Màn Hình:</strong> {manHinh}
          </p>
          <p>
            <strong>Hệ điều hành:</strong> {heDieuHanh}
          </p>
          <p>
            <strong>Camera trước:</strong> {cameraTruoc}
          </p>
          <p>
            <strong>Camera Sau:</strong> {cameraSau}
          </p>
          <p>
            <strong>Ram:</strong> {ram}
          </p>
          <p>
            <strong>Rom:</strong> {rom}
          </p>
          <p>
            <strong>Giá bán:</strong> {giaBan}
          </p>
        </div>
      </div>
    );
  }
}

let mapSateToProps = (state) => {
  return {
    phone: state.phoneReducer.data,
  };
};

export default connect(mapSateToProps)(Detail);
