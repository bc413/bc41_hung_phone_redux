import React, { Component } from "react";
import { connect } from "react-redux";

class ItemPhone extends Component {
  render() {
    let { hinhAnh, tenSP, giaBan, maSP } = this.props.item;
    console.log(this.props);
    return (
      <div className="col-4">
        <div className="card border-primary h-100">
          <img className="card-img-top" src={hinhAnh} alt />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan}</p>
            <button
              onClick={() => {
                this.props.handleChiTiet(maSP);
              }}
              className="btn btn-primary"
            >
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleChiTiet: (idPhone) => {
      let action = {
        type: "CLICK_CHI_TIET",
        payload: idPhone,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemPhone);
