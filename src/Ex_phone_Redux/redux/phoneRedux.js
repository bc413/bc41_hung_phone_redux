import { dataPhoneRedux } from "../Data";

let initiaValue = {
  data: dataPhoneRedux[0],
};

console.log(initiaValue);

export const phoneReducer = (state = initiaValue, action) => {
  switch (action.type) {
    case "CLICK_CHI_TIET": {
      // console.log("chi tiết");
      let index = dataPhoneRedux.findIndex((item) => {
        return item.maSP == action.payload;
      });
      state.data = dataPhoneRedux[index];
      return { ...state };
    }
    default:
      return state;
  }
};
