import { combineReducers } from "redux";
import { phoneReducer } from "./phoneRedux";

export const rootReducer_Ex_Phone_Redux = combineReducers({
  phoneReducer: phoneReducer,
});
// Key quản lý reducer
// value: là tên của reducer mà mình tạo ra
