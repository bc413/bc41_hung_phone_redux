import React, { Component } from "react";
import { dataPhoneRedux } from "./Data";
import ItemPhone from "./ItemPhone";

export default class ListRenderPhone extends Component {
  renderPhone = () => {
    return dataPhoneRedux.map((item, index) => {
      return <ItemPhone key={index} item={item} />;
    });
  };
  render() {
    console.log(dataPhoneRedux);
    return (
      <div>
        <p>ListRenderPhone</p>
        <div className="row">{this.renderPhone()}</div>
      </div>
    );
  }
}
