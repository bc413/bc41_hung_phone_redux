import logo from "./logo.svg";
import "./App.css";
import Ex_phone_Redux from "./Ex_phone_Redux/Ex_phone_Redux";

function App() {
  return (
    <div className="App">
      <Ex_phone_Redux />
    </div>
  );
}

export default App;
